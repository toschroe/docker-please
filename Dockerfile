ARG TORCH_VERSION=latest

FROM pytorch/pytorch:2.0.0-cuda11.7-cudnn8-devel

USER root
RUN apt-get -qq -y update && \
    apt-get -qq -y upgrade && \
    apt-get -qq -y install \
        wget \
        curl \
        git \
        make \
        sudo

COPY requirements_snake.txt .
RUN python3 -m pip install -r requirements_snake.txt

EXPOSE 8888
